package be.testNV.cryptoApp.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.Properties;

public class propertyManager {
    private final static String appPropPath ="application.properties";

    public static void setProperty(String filepath,String name,String value) {
        createFile(filepath);
        Properties properties=new Properties();
        properties.setProperty(name,value);
        try (FileOutputStream fos=new FileOutputStream(filepath)){
            properties.store(fos,null);
            System.out.println("Saved property: "+properties.getProperty(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void setProperty(String name,String value){
        setProperty(appPropPath,name,value);
    }

    private static void createFile(String filepath){
        Path path=Path.of(filepath);
        try {
            if (Files.notExists(path)) {
                Files.createFile(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Properties createProperties(String )
}
