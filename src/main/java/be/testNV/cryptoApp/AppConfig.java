package be.testNV.cryptoApp;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
@ComponentScan
public class AppConfig {
    private String apiKey;

    @Value("${api.key}")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Bean
    @Scope("prototype")
    public <T> ResponseEntity<T> customResponse(){
        String nonce= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yy"+"MM"+"dd"+"HH"+"mm"+"ss"+"hhh"));
        HttpHeaders responseHeaders= new HttpHeaders();
        responseHeaders.add("DEV-SHRIMPY-API-KEY",apiKey);
        responseHeaders.add("DEV-SHRIMPY-API-NONCE",nonce);
        return ResponseEntity.ok().headers(responseHeaders).build();
    }
}
