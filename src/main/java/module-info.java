open module be.testNV.cryptoApp {

    requires java.sql;
    requires java.annotation;
    requires java.xml.bind;
    requires java.validation;

    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.web;

    requires com.sun.xml.bind;
    requires com.fasterxml.jackson.annotation;
    requires org.apache.tomcat.embed.core;
    requires spring.security.config;
    requires spring.security.core;

}