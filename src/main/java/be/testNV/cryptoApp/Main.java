package be.testNV.cryptoApp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx=SpringApplication.run(AppConfig.class,args);
        CryptoApp app=ctx.getBean(CryptoApp.class);
        app.run();
    }

}
