package be.testNV.cryptoApp.exchange;

import be.testNV.cryptoApp.coins.Coin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Exchange implements Serializable {
    private String name;
    private double bestFee;
    private double worstFee;
    private String iconLink;
    private List<Coin> coins=new ArrayList<>();

    public Exchange() {
    }

    public Exchange(String name, double bestFee, double worstFee, String iconLink, List<Coin> coins){
        setName(name);
        setBestFee(bestFee);
        setWorstFee(worstFee);
        setIconLink(iconLink);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBestFee() {
        return bestFee;
    }

    public void setBestFee(double bestFee) {
        this.bestFee = bestFee;
    }

    public double getWorstFee() {
        return worstFee;
    }

    public void setWorstFee(double worstFee) {
        this.worstFee = worstFee;
    }

    public String getIconLink() {
        return iconLink;
    }

    public void setIconLink(String iconLink) {
        this.iconLink = iconLink;
    }

    public List<Coin> getCoins() {
        return coins;
    }

    public void setCoins(List<Coin> coins) {
        this.coins = coins;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exchange exchange = (Exchange) o;
        return Double.compare(exchange.bestFee, bestFee) == 0 && Double.compare(exchange.worstFee, worstFee) == 0 && Objects.equals(name, exchange.name) && Objects.equals(iconLink, exchange.iconLink) && Objects.equals(coins, exchange.coins);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, bestFee, worstFee, iconLink, coins);
    }

    @Override
    public String toString() {
        return "Exchange{" +
                "name='" + name + '\'' +
                ", bestFee=" + bestFee +
                ", worstFee=" + worstFee +
                ", iconLink='" + iconLink + '\'' +
                '}';
    }
}
