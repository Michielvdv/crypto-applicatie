package be.testNV.cryptoApp.coins;

import be.testNV.cryptoApp.chart.CandleChart;
import be.testNV.cryptoApp.chart.LineChart;

public class Coin {
    private String fullName;
    private String shortName;
    private float price;
    private long marketCap;
    private float volume;
    private Byte[] logo;
    private LineChart lineChart;
    private CandleChart candleChart;

    public Coin() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public long getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(long marketCap) {
        this.marketCap = marketCap;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public Byte[] getLogo() {
        return logo;
    }

    public void setLogo(Byte[] logo) {
        this.logo = logo;
    }

    public LineChart getLineChart() {
        return lineChart;
    }

    public void setLineChart(LineChart lineChart) {
        this.lineChart = lineChart;
    }

    public CandleChart getCandleChart() {
        return candleChart;
    }

    public void setCandleChart(CandleChart candleChart) {
        this.candleChart = candleChart;
    }
}
