package be.testNV.cryptoApp;



import be.testNV.cryptoApp.coins.Coin;
import be.testNV.cryptoApp.controller.RestController;
import be.testNV.cryptoApp.exchange.Exchange;
import be.testNV.cryptoApp.exchange.ExchangeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CryptoApp{
    private static ExchangeFactory factory;
    private static RestController controller;
    private static int coinRefreshmentRate;
    private static int exchangeRefreshmentRate;

    public void run(){
        Thread coinThread=new Thread(CryptoApp::updateCoins);
        Thread exchangeThread=new Thread(CryptoApp::updateExchanges);
        coinThread.start();
        exchangeThread.start();
    }

    private static void updateCoins(){
        while (true) {
            try {
                factory.updateCoins(controller.getCoins());
                Thread.sleep(coinRefreshmentRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static void updateExchanges(){
        while (true) {
            try {
                controller.getExchanges();
                Thread.sleep(exchangeRefreshmentRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Autowired
    public void setFactory(ExchangeFactory factory) {
        this.factory = factory;
    }

    public RestController getController() {
        return controller;
    }

    @Autowired
    public void setController(RestController control) {
        controller = control;
    }

    @Value("${data.refreshment.rate.coins}")
    public void setCoinRefreshmentRate(int refreshmentRate) {
        coinRefreshmentRate = refreshmentRate*1000;
    }

    @Value("${data.refreshment.rate.exchanges}")
    public void setExchangeRefreshmentRate(int refreshmentRate) {
        exchangeRefreshmentRate = refreshmentRate*1000;
    }

}
